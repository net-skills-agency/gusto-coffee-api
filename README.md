# Projet Gusto Coffee

## Description
Réalisation d'une application web pour réserver des espaces de coworking et de e-commerce.

## Équipe
Le projet est réalisé par l'agence **Net Skills Agency** composée de David Boyaval, Bastien Domart, Fabien Labbé et Clément Langendorf.

## Gitlab
https://gitlab.com/net-skills-agency

**Projet Back-end :** https://gitlab.com/net-skills-agency/gusto_api  
**Projet Front-end :** https://gitlab.com/net-skills-agency/gusto-coffee-app  

### Description des branches
**Develop :** branche de développement à partir de laquelle nous ferons les branches pour développer de nouvelles fonctionnalités.  
**Staging :** branche pour le serveur de test.  
**Master :**  branche pour la mise en production.  

## Prérequis
- Langage : Php 7
- Framework : Symfony 5
- Gestionnaire de package : Composer
- Base de donnée : MySQL

# Configuration

## Installation
Cloner le projet avec la commande suivante :  
```git clone https://gitlab.com/net-skills-agency/gusto_api.git```

Installation des paquets avec composer avec la commande suivante :  
```composer install```

### Création du fichier .env.local
- Créer un fichier .env.local à la racine du projet à partir de la copie du fichier .env  
- Modifier la ligne suivante avec votre configuration de base de données (**db_user**, **db_password**, **db_name**) :  
```DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7```

### Création de la base de données
- Créer la base de donnée avec la commande suivante :   
```php bin/console doctrine:database:create```  
- Exécuter ensuite une des commandes suivantes pour importer les tables de la base de données :  
```php bin/console doctrine:migrations:migrate``` **ou**  ```php bin/console doctrine:schema:update --force```

## Lancement du projet
Une fois l'installation terminée, vous pouvez tester le projet en exécutant la commande suivante :  
```symfony server:start```  
Aller ensuite sur l'URL indiquée dans votre console (par défaut : http://localhost:8000)

# Tests
### Work in progress
Comment lancer les tests.

Grouper les tests. Pour ne pas lancer systématiquement l'intégralité des tests.

Commencer par des tests de bon fonctionnement de l'environnement :

test connexion bdd
test connexion back : http 200 ou 403 sur l'API
test connexion front : http 200 sur la page d'accueil
test compilation ressources (js, css...) : http 200 sur les ressources en question
tests API externes en ligne + connexion OK sur ces API
...
Automatisation
Make (sauf Windows)
Composer (php)
npm, yarn (js)
Capistrano (orienté déploiement)